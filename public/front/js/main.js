$(function(){
    $('.banner-section__slider').slick({
        dots: true,
        prevArrow: '<button class="banner-section__slider-btn banner-section__slider-btnprev"><img src="front/images/left-arrow-icon.svg" alt=""></button>',
        nextArrow: '<button class="banner-section__slider-btn banner-section__slider-btnnext"><img src="front/images/right-arrow-icon.svg" alt=""></button>',
    });

    $('.tab').on('click', function (e) {
        e.preventDefault();

        $($(this).siblings()).removeClass('tab--active');
        $($(this).parent().siblings().find('div')).removeClass('tabs-content--active');

        $(this).addClass('tab--active');
        $($(this).attr('href')).addClass('tabs-content--active');
    });

    $('.product-item__favorite').on('click', function () {
        $(this).toggleClass('product-item__favorite--active');
    });

    $('.product-slider').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        prevArrow: '<button class="product-slider__slider-btn product-slider__slider-btnprev"><img src="front/images/left-arrow-black-icon.svg" alt=""></button>',
        nextArrow: '<button class="product-slider__slider-btn product-slider__slider-btnnext"><img src="front/images/right-arrow-black-icon.svg" alt=""></button>',
    });

    // styles for checkboxes and dropdowns
    $('.filter-style').styler();

    // раскрытие стрелки. параметр slideToggle можно регулировать скорость раскрытия
    $('.filter__item-drop, .filter__extra').on('click', function () {
        $(this).toggleClass('filter__item-drop--active');
        $(this).next().slideToggle(200);
    });

    // подключает слайдер для цен
    $(".js-range-slider").ionRangeSlider();

    // $('.catalog__filter-button').on('click', function () {
    //     $('.catalog__filter-button').toggleClass('catalog__filter-button--active')
    // });

    $('.catalog__filter_btngrid').on('click', function () {
        $(this).addClass('catalog__filter-button--active');
        $('.catalog__filter_btnline').removeClass('catalog__filter-button--active');
        $('.product-item__wrapper').removeClass('product-item__wrapper--list');
    });

    $('.catalog__filter_btnline').on('click', function () {
        $(this).addClass('catalog__filter-button--active');
        $('.catalog__filter_btngrid').removeClass('catalog__filter-button--active');
        $('.product-item__wrapper').addClass('product-item__wrapper--list');
    });

    // for rating system
    $('.rate-yo').rateYo({
        ratedFill: "#8b0000",
        normalFill: "#C4C4C4",
        spacing: "7px",
        readOnly: true
    });
});
