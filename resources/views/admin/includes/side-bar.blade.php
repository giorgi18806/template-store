<div class="navigation">
    <ul>
        <li>
            <a href="#">
                <span class="icon"><i class="fa fa-apple" aria-hidden="true"></i></span>
                <span class="title"><h2>Brand Name</h2></span>
            </a>
        </li>
        <li>
            <a href="#">
                <span class="icon"><i class="fa fa-tachometer" aria-hidden="true"></i></span>
                <span class="title">Dashboard</span>
            </a>
        </li>
        <li>
            <a href="#">
                <span class="icon"><i class="fa fa-users" aria-hidden="true"></i></span>
                <span class="title">Customers</span>
            </a>
        </li>
        <li>
            <a href="#">
                <span class="icon"><i class="fa fa-tachometer" aria-hidden="true"></i></span>
                <span class="title">Message</span>
            </a>
        </li>
        <li>
            <a href="#">
                <span class="icon"><i class="fa fa-info" aria-hidden="true"></i></span>
                <span class="title">Help</span>
            </a>
        </li>
        <li>
            <a href="#">
                <span class="icon"><i class="fa fa-cog" aria-hidden="true"></i></span>
                <span class="title">Settings</span>
            </a>
        </li>
        <li>
            <a href="#">
                <span class="icon"><i class="fa fa-key" aria-hidden="true"></i></span>
                <span class="title">Password</span>
            </a>
        </li>
        <li>
            <a href="#">
                <span class="icon"><i class="fa fa-sign-out" aria-hidden="true"></i></span>
                <span class="title">Sign Out</span>
            </a>
        </li>
    </ul>
</div>
