<div class="main">
    <div class="topbar">
        <div class="toggle" onclick="toggleMenu();"></div>
        <div class="search">
            <label>
                <input type="text" placeholder="Search here">
                <i class="fa fa-search" aria-hidden="true"></i>
            </label>
        </div>
        <div class="user">
            <img src="img/my-photo.jpg" alt="">
        </div>
    </div>
