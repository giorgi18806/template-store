@include('admin.includes.head')
@include('admin.includes.side-bar')
@include('admin.includes.top-bar')
@yield('main-content')
@include('admin.includes.foot')
