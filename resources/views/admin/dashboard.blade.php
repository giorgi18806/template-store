@extends('admin.layouts.app')

@section('main-content')
    <div class="card-box">
        <div class="card">
            <div>
                <div class="numbers">1,042</div>
                <div class="card-name">Daily Views</div>
            </div>
            <div class="icon-box">
                <i class="fa fa-eye" aria-hidden="true"></i>
            </div>
        </div>
        <div class="card">
            <div>
                <div class="numbers">80</div>
                <div class="card-name">Sales</div>
            </div>
            <div class="icon-box">
                <i class="fa fa-shopping-cart" aria-hidden="true"></i>
            </div>
        </div>
        <div class="card">
            <div>
                <div class="numbers">208</div>
                <div class="card-name">Comments</div>
            </div>
            <div class="icon-box">
                <i class="fa fa-comments" aria-hidden="true"></i>
            </div>
        </div>
        <div class="card">
            <div>
                <div class="numbers">6,042</div>
                <div class="card-name">Earning</div>
            </div>
            <div class="icon-box">
                <i class="fa fa-usd" aria-hidden="true"></i>
            </div>
        </div>
    </div>

    <div class="details">
        <div class="recent-orders">
            <div class="card-header">
                <h2>Recent Orders</h2>
                <a class="btn" href="#">View All</a>
            </div>
            <table>
                <thead>
                <tr>
                    <td>Name</td>
                    <td>Price</td>
                    <td>Payment</td>
                    <td>Status</td>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Star Refrigerator</td>
                    <td>$1200</td>
                    <td>Paid</td>
                    <td><span class="status delivered">Delivered</span></td>
                </tr>
                <tr>
                    <td>Window Coolers</td>
                    <td>$110</td>
                    <td>Due</td>
                    <td><span class="status pending">Pending</span></td>
                </tr>
                <tr>
                    <td>Speakers</td>
                    <td>$620</td>
                    <td>Paid</td>
                    <td><span class="status return">Return</span></td>
                </tr>
                <tr>
                    <td>HP Laptop</td>
                    <td>$6000</td>
                    <td>Due</td>
                    <td><span class="status in-progress">In Progress</span></td>
                </tr>
                <tr>
                    <td>Star Refrigerator</td>
                    <td>$1200</td>
                    <td>Paid</td>
                    <td><span class="status delivered">Delivered</span></td>
                </tr>
                <tr>
                    <td>Window Coolers</td>
                    <td>$110</td>
                    <td>Due</td>
                    <td><span class="status pending">Pending</span></td>
                </tr>
                <tr>
                    <td>Speakers</td>
                    <td>$620</td>
                    <td>Paid</td>
                    <td><span class="status return">Return</span></td>
                </tr>
                <tr>
                    <td>HP Laptop</td>
                    <td>$6000</td>
                    <td>Due</td>
                    <td><span class="status in-progress">In Progress</span></td>
                </tr>
                <tr>
                    <td>Apple Watch</td>
                    <td>$680</td>
                    <td>Due</td>
                    <td><span class="status delivered">Delivered</span></td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="recent-customers">
            <div class="card-header">
                <h2>Recent Customers</h2>
            </div>
            <table>
                <tbody>
                <tr>
                    <td width="60px">
                        <div class="img-box"><img src="img/jamesbond.jfif" alt=""></div>
                    </td>
                    <td><h4>James<br><span>Great Britain</span></h4></td>
                </tr>
                <tr>
                    <td width="60px">
                        <div class="img-box"><img src="img/romanSoldier.png" alt=""></div>
                    </td>
                    <td><h4>Maximus<br><span>Italy</span></h4></td>
                </tr>
                <tr>
                    <td width="60px">
                        <div class="img-box"><img src="img/messi.jpeg" alt=""></div>
                    </td>
                    <td><h4>Lionel<br><span>Argentina</span></h4></td>
                </tr>
                <tr>
                    <td width="60px">
                        <div class="img-box"><img src="img/jean-reno.jpg" alt=""></div>
                    </td>
                    <td><h4>Jean<br><span>France</span></h4></td>
                </tr>
                <tr>
                    <td width="60px">
                        <div class="img-box"><img src="img/jamesbond.jfif" alt=""></div>
                    </td>
                    <td><h4>James<br><span>Italy</span></h4></td>
                </tr>
                <tr>
                    <td width="60px">
                        <div class="img-box"><img src="img/amanda-holden.jpg" alt=""></div>
                    </td>
                    <td><h4>Amanda<br><span>Great Britain</span></h4></td>
                </tr>
                <tr>
                    <td width="60px">
                        <div class="img-box"><img src="img/alesha-dixon.jpg" alt=""></div>
                    </td>
                    <td><h4>Alesha<br><span>Great Britain</span></h4></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection
