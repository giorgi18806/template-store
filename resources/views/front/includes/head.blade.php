<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Barlow:wght@400;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('front/css/normalize.css') }}">
    <link rel="stylesheet" href="{{ asset('front/css/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('front/css/fonts.css') }}">
    <link rel="stylesheet" href="{{ asset('front/css/ion.rangeSlider.min.css') }}">
    <link rel="stylesheet" href="{{ asset('front/css/jquery.formstyler.css') }}">
    <link rel="stylesheet" href="{{ asset('front/css/jquery.formstyler.theme.css') }}">
    <link rel="stylesheet" href="{{ asset('front/css/jquery.rateyo.min.css') }}">
    <link rel="stylesheet" href="{{ asset('front/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('front/css/media.css') }}">
</head>
<body>
