<div class="banner-section__inner">
    <div class="banner-section__slider">
        <a class="banner-section__slider-item" href="#">
            <img class="banner-section__slider-img" src="{{ asset('front/images/banner/banner-slider-1.jpg') }}" alt="">
        </a>
        <a class="banner-section__slider-item" href="#">
            <img class="banner-section__slider-img" src="{{ asset('front/images/banner/banner-slider-2.jpg') }}" alt="">
        </a>
        <a class="banner-section__slider-item" href="#">
            <img class="banner-section__slider-img" src="{{ asset('front/images/banner/banner-slider-3.jpg') }}" alt="">
        </a>
        <a class="banner-section__slider-item" href="#">
            <img class="banner-section__slider-img" src="{{ asset('front/images/banner/banner-slider-4.jpg') }}" alt="">
        </a>
        <a class="banner-section__slider-item" href="#">
            <img class="banner-section__slider-img" src="{{ asset('front/images/banner/banner-slider-5.jpg') }}" alt="">
        </a>
        <a class="banner-section__slider-item" href="#">
            <img class="banner-section__slider-img" src="{{ asset('front/images/banner/banner-slider-6.jpg') }}" alt="">
        </a>
    </div>
    <a class="baner-section__item sale-item" href="#">
        <div class="sale-item__top">
            <div class="sale-item__info">
                Акция
            </div>
            <div class="sale-item__price">
                <div class="price sale-item__price-new">72</div>
                <div class="price sale-item__price-old">85</div>
            </div>
        </div>
        <div class="sale-item__img">
            <img class="" src="{{ asset('front/images/content/sale-1.jpg') }}" alt="">
        </div>
        <h5 class="sale-item__title">
            Соус винный красный 200 гр
        </h5>
        <div class="sale-item__footer">
            Акция действует до
            <span>31.08.2021</span>
        </div>
    </a>
</div>
