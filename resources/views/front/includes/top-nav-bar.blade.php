<header class="header">
    <div class="header__top">
        <div class="container">
            <div class="header__top-inner">
                <nav class="menu">
                    <button class="menu__btn">
                        <span class="menu__btn-line"></span>
                        <span class="menu__btn-line"></span>
                        <span class="menu__btn-line"></span>
                    </button>
                    <ul class="menu__list">
                        <li class="menu__item">
                            <a class="menu__link" href="#">
                                Магазины
                            </a>
                        </li>
                        <li class="menu__item">
                            <a class="menu__link" href="#">
                                Акции
                            </a>
                        </li>
                        <li class="menu__item">
                            <a class="menu__link" href="#">
                                Доставка и оплата
                            </a>
                        </li>
                    </ul>
                </nav>
                <a class="logo" href="#">
                    <img class="logo__img" src="{{asset('front/images/logo_g_u_group.jpg') }}" alt="" height="60">
                </a>
                <div class="header__box">
                    <p class="header__address">
                        ул. Промышленная 5, Вишневое
                    </p>
                    <ul class="user-list">
                        <li class="user-list__item">
                            <a class="user-list__link" href="#">
                                <img src="{{asset('front/images/heart-icon.png') }}" alt="" height="30">
                            </a>
                        </li>
                        <li class="user-list__item">
                            <a class="user-list__link" href="#">
                                <img src="{{asset('front/images/user-icon.png') }}" alt="" height="30">
                            </a>
                        </li>
                        <li class="user-list__item">
                            <a class="user-list__link basket" href="#">
                                <img src="{{asset('front/images/basket-icon.svg') }}" alt="">
                                <p class="basket__num">1</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="header__bottom">
        <div class="container">
            <ul class="menu-categories">
                <li class="menu-categories__item">
                    <a class="menu-categories__link" href="#">
                        Соусы
                    </a>
                </li>
                <li class="menu-categories__item">
                    <a class="menu-categories__link" href="#">
                        Приправы
                    </a>
                </li>
                <li class="menu-categories__item">
                    <a class="menu-categories__link" href="#">
                        Бакалея
                    </a>
                </li>
                <li class="menu-categories__item">
                    <a class="menu-categories__link" href="#">
                        Безалкогольные напитки
                    </a>
                </li>
                <li class="menu-categories__item">
                    <a class="menu-categories__link" href="#">
                        Алкогольные напитки
                    </a>
                </li>
                <li class="menu-categories__item">
                    <a class="menu-categories__link" href="#">
                        Полуфабрикаты
                    </a>
                </li>
                <li class="menu-categories__item">
                    <a class="menu-categories__link" href="#">
                        Мука и крупа
                    </a>
                </li>
                <li class="menu-categories__item">
                    <a class="menu-categories__link" href="#">
                        Грузинские сладости
                    </a>
                </li>
            </ul>
        </div>
    </div>
</header>
