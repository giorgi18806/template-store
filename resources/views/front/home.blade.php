@extends('front.layouts.app')

@section('main-content')

<!--    header section-->


<!--    banner section  -->
<section class="banner-section page-section">
    <div class="container">
        @include('front.includes.banner-sale')
    </div>
</section>

<!-- search tabs section-->
<div class="search page-section">
    <div class="container">
        <div class="search__inner">
            <div class="search__tabs">
                <a class="tab search__tabs-item tab--active" href="#tab-1">Поиск по  номеру</a>
                <a class="tab search__tabs-item" href="#tab-2">Поиск по марке</a>
                <a class="tab search__tabs-item" href="#tab-3">Поиск по названию товара</a>
            </div>
            <div class="search__content">
                <div id="tab-1" class="tabs-content search__content-item tabs-content--active">
                    <form class="search__content-form">
                        <input class="search__content-input" type="text" placeholder="Введите номер ">
                        <button class="search__content-btn" type="submit">искать</button>
                    </form>
                </div>
                <div id="tab-2" class="tabs-content search__content-item">
                    <form class="search__content-form">
                        <input class="search__content-input" type="text" placeholder="Введите марку ">
                        <button class="search__content-btn" type="submit">искать</button>
                    </form>
                </div>
                <div id="tab-3" class="tabs-content search__content-item">
                    <form class="search__content-form">
                        <input class="search__content-input" type="text" placeholder="Введите название товара ">
                        <button class="search__content-btn" type="submit">искать</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!--    categories section  -->
<section class="categories page-section">
    <div class="container">
        <div class="categories__inner">
            <a class="categories__item"  href="{{ route('catalog') }}">
                <div class="categories__item-info">
                    <h4 class="categories__item-title">Соусы</h4>
                    <p class="categories__item-text">Подробнее</p>
                </div>
                <div class="categories__item-img">
                    <img src="{{asset ('front/images/categories-1.png') }}" alt="">
                </div>
            </a>
            <a class="categories__item"  href="{{ route('catalog') }}">
                <div class="categories__item-info">
                    <h4 class="categories__item-title">Приправы</h4>
                    <p class="categories__item-text">Подробнее</p>
                </div>
                <div class="categories__item-img">
                    <img src="{{asset ('front/images/categories-2.png') }}" alt="">
                </div>
            </a>
            <a class="categories__item" href="{{ route('catalog') }}">
                <div class="categories__item-info">
                    <h4 class="categories__item-title">Бакалея</h4>
                    <p class="categories__item-text">Подробнее</p>
                </div>
                <div class="categories__item-img">
                    <img src="{{asset ('front/images/categories-3.png') }}" alt="">
                </div>
            </a>
{{--            <a href="" class="categories__item">--}}
{{--                <div class="categories__item-info">--}}
{{--                    <h4 class="categories__item-title">Безалкогольные напитки</h4>--}}
{{--                    <p class="categories__item-text">Подробнее</p>--}}
{{--                </div>--}}
{{--                <div class="categories__item-img">--}}
{{--                    <img src="{{asset ('front/images/categories-4.png') }}" alt="">--}}
{{--                </div>--}}
{{--            </a>--}}
{{--            <a href="" class="categories__item">--}}
{{--                <div class="categories__item-info">--}}
{{--                    <h4 class="categories__item-title">Алкогольные напитки</h4>--}}
{{--                    <p class="categories__item-text">Подробнее</p>--}}
{{--                </div>--}}
{{--                <div class="categories__item-img">--}}
{{--                    <img src="{{asset ('front/images/categories-5.png') }}" alt="">--}}
{{--                </div>--}}
{{--            </a>--}}
            <a class="categories__item" href="{{ route('catalog') }}">
                <div class="categories__item-info">
                    <h4 class="categories__item-title">Полуфабрикаты</h4>
                    <p class="categories__item-text">Подробнее</p>
                </div>
                <div class="categories__item-img">
                    <img src="{{asset ('front/images/categories-6.png') }}" alt="">
                </div>
            </a>
            <a class="categories__item" href="{{ route('catalog') }}">
                <div class="categories__item-info">
                    <h4 class="categories__item-title">Мука и крупа</h4>
                    <p class="categories__item-text">Подробнее</p>
                </div>
                <div class="categories__item-img">
                    <img src="{{asset ('front/images/categories-6.png') }}" alt="">
                </div>
            </a>
            <a class="categories__item" href="{{ route('catalog') }}">
                <div class="categories__item-info">
                    <h4 class="categories__item-title">Грузинские сладости</h4>
                    <p class="categories__item-text">Подробнее</p>
                </div>
                <div class="categories__item-img">
                    <img src="{{asset ('front/images/categories-6.png') }}" alt="">
                </div>
            </a>
        </div>
    </div>
</section>

<!--    popular products section  -->
<section class="products">
    <div class="container">
        <div class="products__inner">
            <h3 class="product__title">Популярные товары</h3>
            <div class="tabs-wrapper">
                <div class="tabs products__tabs">
                    <a class="tab products__tab tab--active" href="#product-tab-1">запчасти</a>
                    <a class="tab products__tab" href="#product-tab-2">моторы</a>
                    <a class="tab products__tab" href="#product-tab-3">шины </a>
                    <a class="tab products__tab" href="#product-tab-4">электроника</a>
                    <a class="tab products__tab" href="#product-tab-5">инструменты</a>
                    <a class="tab products__tab" href="#product-tab-6">аксессуары</a>
                </div>
                <div class="tabs-container products__container">
                    <div id="product-tab-1" class="tabs-content products__content tabs-content--active">
                        <div class="product-slider">
                            <div class="product-slider__item">
                                <div class="product-item__wrapper">
                                    <button class="product-item__favorite"></button>
                                    <button class="product-item__basket">
                                        <img src="{{ asset('front/images/basket-icon-white.svg') }}" alt="">
                                    </button>
                                    <a class="product-item__notify-link" href="#"><span>Сообщить о поступлении</span></a>
                                    <a class="product-item" href="#">
                                        <p class="product-item__hover-text">посмотреть товар</p>
                                        <img class="product-item__img" src="{{ asset('front/images/content/product-1.png') }}" alt="">
                                        <h4 class="product-item__title">
                                            Водонепроницаемый
                                            Рюкзак
                                        </h4>
                                        <p class="price product-item__price">98 &#8372</p>
                                        <p class="product-item__notify-text">нет в наличии</p>
                                    </a>
                                </div>
                            </div>
                            <div class="product-slider__item">
                                <div class="product-item__wrapper">
                                    <button class="product-item__favorite"></button>
                                    <button class="product-item__basket">
                                        <img src="{{ asset('front/images/basket-icon-white.svg') }}" alt="">
                                    </button>
                                    <a class="product-item__notify-link" href="#"><span>Сообщить о поступлении</span></a>
                                    <a class="product-item product-item--sale" href="#">
                                        <p class="product-item__hover-text">посмотреть товар</p>
                                        <img class="product-item__img" src="{{ asset('front/images/content/product-1.png') }}" alt="">
                                        <h4 class="product-item__title">
                                            Водонепроницаемый
                                            Рюкзак
                                        </h4>
                                        <p class="price product-item__price">98 &#8372</p>
                                        <p class="product-item__notify-text">нет в наличии</p>
                                    </a>
                                </div>
                            </div>
                            <div class="product-slider__item">
                                <div class="product-item__wrapper product-item__not-available">
                                    <button class="product-item__favorite"></button>
                                    <a class="product-item__basket" href="{{ route('product-page') }}">
                                        <img src="{{ asset('front/images/basket-icon-white.svg') }}" alt="">
                                    </a>
                                    <a class="product-item__notify-link" href="#"><span>Сообщить о поступлении</span></a>
                                    <a class="product-item product-item--sale" href="#">
                                        <p class="product-item__hover-text">посмотреть товар</p>
                                        <img class="product-item__img" src="{{ asset('front/images/content/product-1.png') }}" alt="">
                                        <h4 class="product-item__title">
                                            Водонепроницаемый
                                            Рюкзак
                                        </h4>
                                        <p class="price product-item__price">98 &#8372</p>
                                        <p class="product-item__notify-text">нет в наличии</p>
                                    </a>
                                </div>
                            </div>
                            <div class="product-slider__item">
                                <div class="product-item__wrapper">
                                    <button class="product-item__favorite"></button>
                                    <button class="product-item__basket">
                                        <img src="{{ asset('front/images/basket-icon-white.svg') }}" alt="">
                                    </button>
                                    <a class="product-item__notify-link" href="#"><span>Сообщить о поступлении</span></a>
                                    <a class="product-item product-item--sale" href="#">
                                        <p class="product-item__hover-text">посмотреть товар</p>
                                        <img class="product-item__img" src="{{ asset('front/images/content/product-1.png') }}" alt="">
                                        <h4 class="product-item__title">
                                            Водонепроницаемый
                                            Рюкзак
                                        </h4>
                                        <p class="price product-item__price">98 &#8372</p>
                                        <p class="product-item__notify-text">нет в наличии</p>
                                    </a>
                                </div>
                            </div>
                            <div class="product-slider__item">
                                <div class="product-item__wrapper">
                                    <button class="product-item__favorite"></button>
                                    <button class="product-item__basket">
                                        <img src="{{ asset('front/images/basket-icon-white.svg') }}" alt="">
                                    </button>
                                    <a class="product-item__notify-link" href="#"><span>Сообщить о поступлении</span></a>
                                    <a class="product-item product-item--sale" href="#">
                                        <p class="product-item__hover-text">посмотреть товар</p>
                                        <img class="product-item__img" src="{{ asset('front/images/content/product-2.png') }}" alt="">
                                        <h4 class="product-item__title">
                                            Водонепроницаемый
                                            Рюкзак
                                        </h4>
                                        <p class="price product-item__price">98 &#8372</p>
                                        <p class="product-item__notify-text">нет в наличии</p>
                                    </a>
                                </div>
                            </div>
                            <div class="product-slider__item">
                                <div class="product-item__wrapper">
                                    <button class="product-item__favorite"></button>
                                    <button class="product-item__basket">
                                        <img src="{{ asset('front/images/basket-icon-white.svg') }}" alt="">
                                    </button>
                                    <a class="product-item__notify-link" href="#"><span>Сообщить о поступлении</span></a>
                                    <a class="product-item product-item--sale" href="#">
                                        <p class="product-item__hover-text">посмотреть товар</p>
                                        <img class="product-item__img" src="{{ asset('front/images/content/product-1.png') }}" alt="">
                                        <h4 class="product-item__title">
                                            Водонепроницаемый
                                            Рюкзак
                                        </h4>
                                        <p class="price product-item__price">98 &#8372</p>
                                        <p class="product-item__notify-text">нет в наличии</p>
                                    </a>
                                </div>
                            </div>
                            <div class="product-slider__item">
                                <div class="product-item__wrapper">
                                    <button class="product-item__favorite"></button>
                                    <button class="product-item__basket">
                                        <img src="{{ asset('front/images/basket-icon-white.svg') }}" alt="">
                                    </button>
                                    <a class="product-item__notify-link" href="#"><span>Сообщить о поступлении</span></a>
                                    <a class="product-item product-item--sale" href="#">
                                        <p class="product-item__hover-text">посмотреть товар</p>
                                        <img class="product-item__img" src="{{ asset('front/images/content/product-1.png') }}" alt="">
                                        <h4 class="product-item__title">
                                            Водонепроницаемый
                                            Рюкзак
                                        </h4>
                                        <p class="price product-item__price">98 &#8372</p>
                                        <p class="product-item__notify-text">нет в наличии</p>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="product-tab-2" class="tabs-content products__content">
                        <div class="product-slider">
                            <div class="product-slider__item">
                                <div class="product-item__wrapper">
                                    <button class="product-item__favorite"></button>
                                    <button class="product-item__basket">
                                        <img src="{{ asset('front/images/basket-icon-white.svg') }}" alt="">
                                    </button>
                                    <a class="product-item__notify-link" href="#"><span>Сообщить о поступлении</span></a>
                                    <a class="product-item" href="#">
                                        <p class="product-item__hover-text">посмотреть товар</p>
                                        <img class="product-item__img" src="{{ asset('front/images/content/product-2.png') }}" alt="">
                                        <h4 class="product-item__title">
                                            Водонепроницаемый
                                            Рюкзак
                                        </h4>
                                        <p class="price product-item__price">98 &#8372</p>
                                        <p class="product-item__notify-text">нет в наличии</p>
                                    </a>
                                </div>
                            </div>
                            <div class="product-slider__item">
                                <div class="product-item__wrapper">
                                    <button class="product-item__favorite"></button>
                                    <button class="product-item__basket">
                                        <img src="{{ asset('front/images/basket-icon-white.svg') }}" alt="">
                                    </button>
                                    <a class="product-item__notify-link" href="#"><span>Сообщить о поступлении</span></a>
                                    <a class="product-item product-item--sale" href="#">
                                        <p class="product-item__hover-text">посмотреть товар</p>
                                        <img class="product-item__img" src="{{ asset('front/images/content/product-2.png') }}" alt="">
                                        <h4 class="product-item__title">
                                            Водонепроницаемый
                                            Рюкзак
                                        </h4>
                                        <p class="price product-item__price">98 &#8372</p>
                                        <p class="product-item__notify-text">нет в наличии</p>
                                    </a>
                                </div>
                            </div>
                            <div class="product-slider__item">
                                <div class="product-item__wrapper product-item__not-available">
                                    <button class="product-item__favorite"></button>
                                    <button class="product-item__basket">
                                        <img src="{{ asset('front/images/basket-icon-white.svg') }}" alt="">
                                    </button>
                                    <a class="product-item__notify-link" href="#"><span>Сообщить о поступлении</span></a>
                                    <a class="product-item product-item--sale" href="#">
                                        <p class="product-item__hover-text">посмотреть товар</p>
                                        <img class="product-item__img" src="{{ asset('front/images/content/product-2.png') }}" alt="">
                                        <h4 class="product-item__title">
                                            Водонепроницаемый
                                            Рюкзак
                                        </h4>
                                        <p class="price product-item__price">98 &#8372</p>
                                        <p class="product-item__notify-text">нет в наличии</p>
                                    </a>
                                </div>
                            </div>
                            <div class="product-slider__item">
                                <div class="product-item__wrapper">
                                    <button class="product-item__favorite"></button>
                                    <button class="product-item__basket">
                                        <img src="{{ asset('front/images/basket-icon-white.svg') }}" alt="">
                                    </button>
                                    <a class="product-item__notify-link" href="#"><span>Сообщить о поступлении</span></a>
                                    <a class="product-item product-item--sale" href="#">
                                        <p class="product-item__hover-text">посмотреть товар</p>
                                        <img class="product-item__img" src="{{ asset('front/images/content/product-2.png') }}" alt="">
                                        <h4 class="product-item__title">
                                            Водонепроницаемый
                                            Рюкзак
                                        </h4>
                                        <p class="price product-item__price">98 &#8372</p>
                                        <p class="product-item__notify-text">нет в наличии</p>
                                    </a>
                                </div>
                            </div>
                            <div class="product-slider__item">
                                <div class="product-item__wrapper">
                                    <button class="product-item__favorite"></button>
                                    <button class="product-item__basket">
                                        <img src="{{ asset('front/images/basket-icon-white.svg') }}" alt="">
                                    </button>
                                    <a class="product-item__notify-link" href="#"><span>Сообщить о поступлении</span></a>
                                    <a class="product-item product-item--sale" href="#">
                                        <p class="product-item__hover-text">посмотреть товар</p>
                                        <img class="product-item__img" src="{{ asset('front/images/content/product-2.png') }}" alt="">
                                        <h4 class="product-item__title">
                                            Водонепроницаемый
                                            Рюкзак
                                        </h4>
                                        <p class="price product-item__price">98 &#8372</p>
                                        <p class="product-item__notify-text">нет в наличии</p>
                                    </a>
                                </div>
                            </div>
                            <div class="product-slider__item">
                                <div class="product-item__wrapper">
                                    <button class="product-item__favorite"></button>
                                    <button class="product-item__basket">
                                        <img src="{{ asset('front/images/basket-icon-white.svg') }}" alt="">
                                    </button>
                                    <a class="product-item__notify-link" href="#"><span>Сообщить о поступлении</span></a>
                                    <a class="product-item product-item--sale" href="#">
                                        <p class="product-item__hover-text">посмотреть товар</p>
                                        <img class="product-item__img" src="{{ asset('front/images/content/product-2.png') }}" alt="">
                                        <h4 class="product-item__title">
                                            Водонепроницаемый
                                            Рюкзак
                                        </h4>
                                        <p class="price product-item__price">98 &#8372</p>
                                        <p class="product-item__notify-text">нет в наличии</p>
                                    </a>
                                </div>
                            </div>
                            <div class="product-slider__item">
                                <div class="product-item__wrapper">
                                    <button class="product-item__favorite"></button>
                                    <button class="product-item__basket">
                                        <img src="{{ asset('front/images/basket-icon-white.svg') }}" alt="">
                                    </button>
                                    <a class="product-item__notify-link" href="#"><span>Сообщить о поступлении</span></a>
                                    <a class="product-item product-item--sale" href="#">
                                        <p class="product-item__hover-text">посмотреть товар</p>
                                        <img class="product-item__img" src="{{ asset('front/images/content/product-2.png') }}" alt="">
                                        <h4 class="product-item__title">
                                            Водонепроницаемый
                                            Рюкзак
                                        </h4>
                                        <p class="price product-item__price">98 &#8372</p>
                                        <p class="product-item__notify-text">нет в наличии</p>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="product-tab-3" class="tabs-content products__content">Slider 3</div>
                    <div id="product-tab-4" class="tabs-content products__content">Slider 4</div>
                    <div id="product-tab-5" class="tabs-content products__content">Slider 5</div>
                    <div id="product-tab-6" class="tabs-content products__content">Slider 6</div>
                </div>
            </div>
            <div class="product__more">
                <a class="product__more-link" href="#">Показать еще</a>
            </div>
        </div>
    </div>
</section>

<!--    banner section   -->
<div class="banner">
    <div class="container">
        <a class="banner__link" href="">
            <img src="{{ asset('front/images/content/banner.jpg') }}" alt="">
        </a>
    </div>
</div>

<!--    related products section  -->
<section class="products">
    <div class="container">
        <div class="products__inner">
            <h3 class="product__title">С этим товаром покупают</h3>
            <div class="tabs-wrapper">
                <div class="tabs products__tabs">
                    <a class="tab products__tab tab--active" href="#related-tab-1">запчасти</a>
                    <a class="tab products__tab" href="#related-tab-2">моторы</a>
                    <a class="tab products__tab" href="#related-tab-3">шины </a>
                    <a class="tab products__tab" href="#related-tab-4">электроника</a>
                    <a class="tab products__tab" href="#related-tab-5">инструменты</a>
                    <a class="tab products__tab" href="#related-tab-6">аксессуары</a>
                </div>
                <div class="tabs-container products__container">
                    <div id="related-tab-1" class="tabs-content products__content tabs-content--active">
                        <div class="product-slider">
                            <div class="product-slider__item">
                                <div class="product-item__wrapper">
                                    <button class="product-item__favorite"></button>
                                    <button class="product-item__basket">
                                        <img src="{{ asset('front/images/basket-icon-white.svg') }}" alt="">
                                    </button>
                                    <a class="product-item__notify-link" href="#"><span>Сообщить о поступлении</span></a>
                                    <a class="product-item" href="#">
                                        <p class="product-item__hover-text">посмотреть товар</p>
                                        <img class="product-item__img" src="{{ asset('front/images/content/product-1.png') }}" alt="">
                                        <h4 class="product-item__title">
                                            Водонепроницаемый
                                            Рюкзак
                                        </h4>
                                        <p class="price product-item__price">98 &#8372</p>
                                        <p class="product-item__notify-text">нет в наличии</p>
                                    </a>
                                </div>
                            </div>
                            <div class="product-slider__item">
                                <div class="product-item__wrapper">
                                    <button class="product-item__favorite"></button>
                                    <button class="product-item__basket">
                                        <img src="{{ asset('front/images/basket-icon-white.svg') }}" alt="">
                                    </button>
                                    <a class="product-item__notify-link" href="#"><span>Сообщить о поступлении</span></a>
                                    <a class="product-item product-item--sale" href="#">
                                        <p class="product-item__hover-text">посмотреть товар</p>
                                        <img class="product-item__img" src="{{ asset('front/images/content/product-1.png') }}" alt="">
                                        <h4 class="product-item__title">
                                            Водонепроницаемый
                                            Рюкзак
                                        </h4>
                                        <p class="price product-item__price">98 &#8372</p>
                                        <p class="product-item__notify-text">нет в наличии</p>
                                    </a>
                                </div>
                            </div>
                            <div class="product-slider__item">
                                <div class="product-item__wrapper product-item__not-available">
                                    <button class="product-item__favorite"></button>
                                    <button class="product-item__basket">
                                        <img src="{{ ('front/images/basket-icon-white.svg') }}" alt="">
                                    </button>
                                    <a class="product-item__notify-link" href="#"><span>Сообщить о поступлении</span></a>
                                    <a class="product-item product-item--sale" href="#">
                                        <p class="product-item__hover-text">посмотреть товар</p>
                                        <img class="product-item__img" src="{{ ('front/images/content/product-1.png') }}" alt="">
                                        <h4 class="product-item__title">
                                            Водонепроницаемый
                                            Рюкзак
                                        </h4>
                                        <p class="price product-item__price">98 &#8372</p>
                                        <p class="product-item__notify-text">нет в наличии</p>
                                    </a>
                                </div>
                            </div>
                            <div class="product-slider__item">
                                <div class="product-item__wrapper">
                                    <button class="product-item__favorite"></button>
                                    <button class="product-item__basket">
                                        <img src="{{ ('front/images/basket-icon-white.svg') }}" alt="">
                                    </button>
                                    <a class="product-item__notify-link" href="#"><span>Сообщить о поступлении</span></a>
                                    <a class="product-item product-item--sale" href="#">
                                        <p class="product-item__hover-text">посмотреть товар</p>
                                        <img class="product-item__img" src="{{ ('front/images/content/product-1.png') }}" alt="">
                                        <h4 class="product-item__title">
                                            Водонепроницаемый
                                            Рюкзак
                                        </h4>
                                        <p class="price product-item__price">98 &#8372</p>
                                        <p class="product-item__notify-text">нет в наличии</p>
                                    </a>
                                </div>
                            </div>
                            <div class="product-slider__item">
                                <div class="product-item__wrapper">
                                    <button class="product-item__favorite"></button>
                                    <button class="product-item__basket">
                                        <img src="{{ asset('front/images/basket-icon-white.svg') }}" alt="">
                                    </button>
                                    <a class="product-item__notify-link" href="#"><span>Сообщить о поступлении</span></a>
                                    <a class="product-item product-item--sale" href="#">
                                        <p class="product-item__hover-text">посмотреть товар</p>
                                        <img class="product-item__img" src="{{ asset('front/images/content/product-2.png') }}" alt="">
                                        <h4 class="product-item__title">
                                            Водонепроницаемый
                                            Рюкзак
                                        </h4>
                                        <p class="price product-item__price">98 &#8372</p>
                                        <p class="product-item__notify-text">нет в наличии</p>
                                    </a>
                                </div>
                            </div>
                            <div class="product-slider__item">
                                <div class="product-item__wrapper">
                                    <button class="product-item__favorite"></button>
                                    <button class="product-item__basket">
                                        <img src="{{ asset('front/images/basket-icon-white.svg') }}" alt="">
                                    </button>
                                    <a class="product-item__notify-link" href="#"><span>Сообщить о поступлении</span></a>
                                    <a class="product-item product-item--sale" href="#">
                                        <p class="product-item__hover-text">посмотреть товар</p>
                                        <img class="product-item__img" src="{{ asset('front/images/content/product-1.png') }}" alt="">
                                        <h4 class="product-item__title">
                                            Водонепроницаемый
                                            Рюкзак
                                        </h4>
                                        <p class="price product-item__price">98 &#8372</p>
                                        <p class="product-item__notify-text">нет в наличии</p>
                                    </a>
                                </div>
                            </div>
                            <div class="product-slider__item">
                                <div class="product-item__wrapper">
                                    <button class="product-item__favorite"></button>
                                    <button class="product-item__basket">
                                        <img src="{{ asset('front/images/basket-icon-white.svg') }}" alt="">
                                    </button>
                                    <a class="product-item__notify-link" href="#"><span>Сообщить о поступлении</span></a>
                                    <a class="product-item product-item--sale" href="#">
                                        <p class="product-item__hover-text">посмотреть товар</p>
                                        <img class="product-item__img" src="{{ asset('front/images/content/product-1.png') }}" alt="">
                                        <h4 class="product-item__title">
                                            Водонепроницаемый
                                            Рюкзак
                                        </h4>
                                        <p class="price product-item__price">98 &#8372</p>
                                        <p class="product-item__notify-text">нет в наличии</p>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="related-tab-2" class="tabs-content products__content">
                        <div class="product-slider">
                            <div class="product-slider__item">
                                <div class="product-item__wrapper">
                                    <button class="product-item__favorite"></button>
                                    <button class="product-item__basket">
                                        <img src="{{ asset('front/images/basket-icon-white.svg') }}" alt="">
                                    </button>
                                    <a class="product-item__notify-link" href="#"><span>Сообщить о поступлении</span></a>
                                    <a class="product-item" href="#">
                                        <p class="product-item__hover-text">посмотреть товар</p>
                                        <img class="product-item__img" src="{{ asset('front/images/content/product-2.png') }}" alt="">
                                        <h4 class="product-item__title">
                                            Водонепроницаемый
                                            Рюкзак
                                        </h4>
                                        <p class="price product-item__price">98 &#8372</p>
                                        <p class="product-item__notify-text">нет в наличии</p>
                                    </a>
                                </div>
                            </div>
                            <div class="product-slider__item">
                                <div class="product-item__wrapper">
                                    <button class="product-item__favorite"></button>
                                    <button class="product-item__basket">
                                        <img src="{{ asset('front/images/basket-icon-white.svg') }}" alt="">
                                    </button>
                                    <a class="product-item__notify-link" href="#"><span>Сообщить о поступлении</span></a>
                                    <a class="product-item product-item--sale" href="#">
                                        <p class="product-item__hover-text">посмотреть товар</p>
                                        <img class="product-item__img" src="{{ ('front/images/content/product-2.png') }}" alt="">
                                        <h4 class="product-item__title">
                                            Водонепроницаемый
                                            Рюкзак
                                        </h4>
                                        <p class="price product-item__price">98 &#8372</p>
                                        <p class="product-item__notify-text">нет в наличии</p>
                                    </a>
                                </div>
                            </div>
                            <div class="product-slider__item">
                                <div class="product-item__wrapper product-item__not-available">
                                    <button class="product-item__favorite"></button>
                                    <button class="product-item__basket">
                                        <img src="{{ asset('front/images/basket-icon-white.svg') }}" alt="">
                                    </button>
                                    <a class="product-item__notify-link" href="#"><span>Сообщить о поступлении</span></a>
                                    <a class="product-item product-item--sale" href="#">
                                        <p class="product-item__hover-text">посмотреть товар</p>
                                        <img class="product-item__img" src="{{ ('front/images/content/product-2.png') }}" alt="">
                                        <h4 class="product-item__title">
                                            Водонепроницаемый
                                            Рюкзак
                                        </h4>
                                        <p class="price product-item__price">98 &#8372</p>
                                        <p class="product-item__notify-text">нет в наличии</p>
                                    </a>
                                </div>
                            </div>
                            <div class="product-slider__item">
                                <div class="product-item__wrapper">
                                    <button class="product-item__favorite"></button>
                                    <button class="product-item__basket">
                                        <img src="{{ asset('front/images/basket-icon-white.svg') }}" alt="">
                                    </button>
                                    <a class="product-item__notify-link" href="#"><span>Сообщить о поступлении</span></a>
                                    <a class="product-item product-item--sale" href="#">
                                        <p class="product-item__hover-text">посмотреть товар</p>
                                        <img class="product-item__img" src="{{ asset('front/images/content/product-2.png') }}" alt="">
                                        <h4 class="product-item__title">
                                            Водонепроницаемый
                                            Рюкзак
                                        </h4>
                                        <p class="price product-item__price">98 &#8372</p>
                                        <p class="product-item__notify-text">нет в наличии</p>
                                    </a>
                                </div>
                            </div>
                            <div class="product-slider__item">
                                <div class="product-item__wrapper">
                                    <button class="product-item__favorite"></button>
                                    <button class="product-item__basket">
                                        <img src="{{ asset('front/images/basket-icon-white.svg') }}" alt="">
                                    </button>
                                    <a class="product-item__notify-link" href="#"><span>Сообщить о поступлении</span></a>
                                    <a class="product-item product-item--sale" href="#">
                                        <p class="product-item__hover-text">посмотреть товар</p>
                                        <img class="product-item__img" src="{{ asset('front/images/content/product-2.png') }}" alt="">
                                        <h4 class="product-item__title">
                                            Водонепроницаемый
                                            Рюкзак
                                        </h4>
                                        <p class="price product-item__price">98 &#8372</p>
                                        <p class="product-item__notify-text">нет в наличии</p>
                                    </a>
                                </div>
                            </div>
                            <div class="product-slider__item">
                                <div class="product-item__wrapper">
                                    <button class="product-item__favorite"></button>
                                    <button class="product-item__basket">
                                        <img src="{{ asset('front/images/basket-icon-white.svg') }}" alt="">
                                    </button>
                                    <a class="product-item__notify-link" href="#"><span>Сообщить о поступлении</span></a>
                                    <a class="product-item product-item--sale" href="#">
                                        <p class="product-item__hover-text">посмотреть товар</p>
                                        <img class="product-item__img" src="{{ asset('front/images/content/product-2.png') }}" alt="">
                                        <h4 class="product-item__title">
                                            Водонепроницаемый
                                            Рюкзак
                                        </h4>
                                        <p class="price product-item__price">98 &#8372</p>
                                        <p class="product-item__notify-text">нет в наличии</p>
                                    </a>
                                </div>
                            </div>
                            <div class="product-slider__item">
                                <div class="product-item__wrapper">
                                    <button class="product-item__favorite"></button>
                                    <button class="product-item__basket">
                                        <img src="{{ asset('front/images/basket-icon-white.svg') }}" alt="">
                                    </button>
                                    <a class="product-item__notify-link" href="#"><span>Сообщить о поступлении</span></a>
                                    <a class="product-item product-item--sale" href="#">
                                        <p class="product-item__hover-text">посмотреть товар</p>
                                        <img class="product-item__img" src="{{ asset('front/images/content/product-2.png') }}" alt="">
                                        <h4 class="product-item__title">
                                            Водонепроницаемый
                                            Рюкзак
                                        </h4>
                                        <p class="price product-item__price">98 &#8372</p>
                                        <p class="product-item__notify-text">нет в наличии</p>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="related-tab-3" class="tabs-content products__content">Slider 3</div>
                    <div id="related-tab-4" class="tabs-content products__content">Slider 4</div>
                    <div id="related-tab-5" class="tabs-content products__content">Slider 5</div>
                    <div id="related-tab-6" class="tabs-content products__content">Slider 6</div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
