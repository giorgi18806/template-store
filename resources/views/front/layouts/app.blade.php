@include('front.includes.head')
<body>
@include('front.includes.top-nav-bar')
@yield('main-content')
@include('front.includes.footer')
@include('front.includes.foot')
</body>
