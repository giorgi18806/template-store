@extends('front.layouts.app')

@section('main-content')
    <!--    breadcrumbs section-->
    <div class="breadcrumbs">
        <div class="container">
            <ul class="breadcrumbs__list">
                <li class="breadcrumbs__list-item">
                    <a href="#">Главная</a>
                </li>
                <li class="breadcrumbs__list-item">
                    <a href="#">Гидроциклы</a>
                </li>
                <li class="breadcrumbs__list-item">
                    <span>Гидроцикл BRP SeaDoo GTI 155hp SE Long Blue Metallic</span>
                </li>
            </ul>
        </div>
    </div>

    <!--    main section-->
    <section class="product-card">
        <div class="container">
            <div class="product-card__inner">
                <div class="product-card__img-box product-item--sale">
                    <img class="product-card__img"  src="{{ asset('front/images/content/gidrotsikl-large.png') }}" alt="">
                    <p class="product-card__price-old">37 848 &#8372</p>
                    <p class="product-card__price-new">20 848 &#8372</p>
                    <a class="product-card__link" href="#">
                        Нашли дешевле? Снизим цену!
                    </a>
                </div>
                <div class="product-card__content">
                    <h1 class="product-card__title">
                        Гидроцикл BRP SeaDoo GTI 155hp SE Long Blue Metallic
                    </h1>
                    <p class="product-card__code">Код товара: 377777-2</p>
                    <div class="product-card__buttons">
                        <a class="product-card__icon-favorite" href="#">
                            <img src="{{ asset('front/images/favorite.svg') }}" alt="">
                        </a>
                        <a class="product-card__icon-comparision" href="#">
                            <img src="{{ asset('front/images/comparision.svg') }}" alt="">
                        </a>
                        <a class="rate" href="#">
                            <div class="rate-yo" data-rateyo-rating="4"></div>
                        </a>
                    </div>
                    <div class="tabs-wrapper product-card__tabs">
                        <div class="tabs">
                            <a class="tab product-card__tab tab--active" href="#product-tab-1">Характеристики</a>
                            <a class="tab product-card__tab" href="#product-tab-2">Наличие в магазине</a>
                        </div>
                        <div class="tabs-container">
                            <div class="tabs-content product-card__tab-content tabs-content--active" id="product-tab-1">
                                <ul class="product-card__list">
                                    <li class="product-card__item">
                                        <div class="product-card__item-left">Производитель</div>
                                        <div class="product-card__item-right">Канада</div>
                                    </li>
                                    <li class="product-card__item">
                                        <div class="product-card__item-left">Количество мест, шт: </div>
                                        <div class="product-card__item-right">3</div>
                                    </li>
                                    <li class="product-card__item">
                                        <div class="product-card__item-left">ПроизМощность, л.с.водитель</div>
                                        <div class="product-card__item-right">155</div>
                                    </li>
                                    <li class="product-card__item">
                                        <div class="product-card__item-left">Тип двигателя</div>
                                        <div class="product-card__item-right">Бензиновый</div>
                                    </li>
                                    <li class="product-card__item">
                                        <div class="product-card__item-left">Год выпуска</div>
                                        <div class="product-card__item-right">2018</div>
                                    </li>
                                </ul>
                                <a class="product-card__more" href="#">Показать еще</a>
                                <div class="product-card__btn">
                                    <button class="product-card__btn">купить</button>
                                </div>
                            </div>
                            <div class="tabs-content product-card__tab-content" id="product-tab-2">content-2</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="card__tabs">
        <div class="container">
            <div class="tabs-wrapper">
                <div class="tabs card__tab-box">
                    <a class="tab card__tab" href="#product-details-tab-1">О товаре</a>
                    <a class="tab card__tab" href="#product-details-tab-2">Характеристики</a>
                    <a class="tab card__tab" href="#product-details-tab-3">Отзывы</a>
                    <a class="tab card__tab tab--active" href="#product-details-tab-4">Самовывоз</a>
                    <a class="tab card__tab" href="#product-details-tab-5">Доставка</a>
                    <a class="tab card__tab" href="#product-details-tab-6">Cервис</a>
                    <a class="tab card__tab" href="#product-details-tab-7">Гарантия</a>
                </div>
                <div class="tabs-container">
                    <div class="card__tab-content tabs-content" id="product-details-tab-1">content1</div>
                    <div class="card__tab-content tabs-content" id="product-details-tab-2">content2</div>
                    <div class="card__tab-content tabs-content" id="product-details-tab-3">content3</div>
                    <div class="card__tab-content tabs-content tabs-content--active" id="product-details-tab-4">
                        <form>
                            <div class="card__top-line">
                                <label class="card__label-search">
                                    Магазин
                                    <input class="card__input-search" type="text">
                                </label>
                                <label class="card__label-pickup">
                                    <input class="filter-style" type="radio" name="pickup" checked>
                                    Забрать сегодня
                                </label>
                                <label class="card__label-pickup">
                                    <input class="filter-style" type="radio" name="pickup">
                                    Забрать в течение недели
                                </label>
                            </div>
                            <ul class="card__list">
                                <li class="card__list-item card__list-itemtitle">
                                    <div class="card__list-address">Адрес</div>
                                    <div class="card__list-workhours">Режим работы</div>
                                    <div class="card__list-available">Доступно</div>
                                    <div class="card__list-num">Количество</div>
                                    <div class="card__list-btn"></div>
                                </li>
                                <li class="card__list-item">
                                    <div class="card__list-address">Москва, ул. Науки 25</div>
                                    <div class="card__list-workhours">
                                        <div class="workhours">
                                            <span>пн-сб:</span><span>08:00-19:00</span>
                                        </div>
                                        <div class="workhours">
                                            <span>вс:</span><span>09:00-17:00</span>
                                        </div>
                                    </div>
                                    <div class="card__list-available">В наличии</div>
                                    <div class="card__list-num">1</div>
                                    <div class="card__list-btn">
                                        <button type="submit">купить</button>
                                    </div>
                                </li>
                                <li class="card__list-item">
                                    <div class="card__list-address">Москва, ул.  Южная 134</div>
                                    <div class="card__list-workhours">
                                        <div class="workhours">
                                            <span>пн-сб:</span><span>08:00-19:00</span>
                                        </div>
                                        <div class="workhours">
                                            <span>вс:</span><span>09:00-17:00</span>
                                        </div>
                                    </div>
                                    <div class="card__list-available">В наличии</div>
                                    <div class="card__list-num">2</div>
                                    <div class="card__list-btn">
                                        <button type="submit">купить</button>
                                    </div>
                                </li>
                                <li class="card__list-item">
                                    <div class="card__list-address">Санкт-Петербург, <br>
                                        ул. Красная 19</div>
                                    <div class="card__list-workhours">
                                        <div class="workhours">
                                            <span>пн-сб:</span><span>08:00-19:00</span>
                                        </div>
                                        <div class="workhours">
                                            <span>вс:</span><span>09:00-17:00</span>
                                        </div>
                                    </div>
                                    <div class="card__list-available">Нет в наличии</div>
                                    <div class="card__list-num">0</div>
                                    <div class="card__list-btn">
                                        <button type="submit">купить</button>
                                    </div>
                                </li>
                                <li class="card__list-item">
                                    <div class="card__list-address">Киев, ул Шевченко 167</div>
                                    <div class="card__list-workhours">
                                        <div class="workhours">
                                            <span>пн-сб:</span><span>08:00-19:00</span>
                                        </div>
                                        <div class="workhours">
                                            <span>вс:</span><span>09:00-17:00</span>
                                        </div>
                                    </div>
                                    <div class="card__list-available">Нет в наличии</div>
                                    <div class="card__list-num">0</div>
                                    <div class="card__list-btn">
                                        <button type="submit">купить</button>
                                    </div>
                                </li>
                            </ul>
                        </form>
                    </div>
                    <div class="card__tab-content tabs-content" id="product-details-tab-5">content5</div>
                    <div class="card__tab-content tabs-content" id="product-details-tab-6">content6</div>
                    <div class="card__tab-content tabs-content" id="product-details-tab-7">content7</div>
                </div>
            </div>
        </div>
    </section>

    <!--    related products section  -->
    <section class="products">
        <div class="container">
            <div class="products__inner">
                <h3 class="product__title">С этим товаром покупают</h3>
                <div class="tabs-wrapper">
                    <div class="tabs products__tabs">
                        <a class="tab products__tab tab--active" href="#related-tab-1">запчасти</a>
                        <a class="tab products__tab" href="#related-tab-2">моторы</a>
                        <a class="tab products__tab" href="#related-tab-3">шины </a>
                        <a class="tab products__tab" href="#related-tab-4">электроника</a>
                        <a class="tab products__tab" href="#related-tab-5">инструменты</a>
                        <a class="tab products__tab" href="#related-tab-6">аксессуары</a>
                    </div>
                    <div class="tabs-container products__container">
                        <div id="related-tab-1" class="tabs-content products__content tabs-content--active">
                            <div class="product-slider">
                                <div class="product-slider__item">
                                    <div class="product-item__wrapper">
                                        <button class="product-item__favorite"></button>
                                        <button class="product-item__basket">
                                            <img src="{{ asset('front/images/basket-icon-white.svg') }}" alt="">
                                        </button>
                                        <a class="product-item__notify-link" href="#"><span>Сообщить о поступлении</span></a>
                                        <a class="product-item" href="#">
                                            <p class="product-item__hover-text">посмотреть товар</p>
                                            <img class="product-item__img" src="{{ asset('front/images/content/product-1.png') }}" alt="">
                                            <h4 class="product-item__title">
                                                Водонепроницаемый
                                                Рюкзак
                                            </h4>
                                            <p class="price product-item__price">98 &#8372</p>
                                            <p class="product-item__notify-text">нет в наличии</p>
                                        </a>
                                    </div>
                                </div>
                                <div class="product-slider__item">
                                    <div class="product-item__wrapper">
                                        <button class="product-item__favorite"></button>
                                        <button class="product-item__basket">
                                            <img src="{{ asset('front/images/basket-icon-white.svg') }}" alt="">
                                        </button>
                                        <a class="product-item__notify-link" href="#"><span>Сообщить о поступлении</span></a>
                                        <a class="product-item product-item--sale" href="#">
                                            <p class="product-item__hover-text">посмотреть товар</p>
                                            <img class="product-item__img" src="{{ asset('front/images/content/product-1.png') }}" alt="">
                                            <h4 class="product-item__title">
                                                Водонепроницаемый
                                                Рюкзак
                                            </h4>
                                            <p class="price product-item__price">98 &#8372</p>
                                            <p class="product-item__notify-text">нет в наличии</p>
                                        </a>
                                    </div>
                                </div>
                                <div class="product-slider__item">
                                    <div class="product-item__wrapper product-item__not-available">
                                        <button class="product-item__favorite"></button>
                                        <button class="product-item__basket">
                                            <img src="{{ ('front/images/basket-icon-white.svg') }}" alt="">
                                        </button>
                                        <a class="product-item__notify-link" href="#"><span>Сообщить о поступлении</span></a>
                                        <a class="product-item product-item--sale" href="#">
                                            <p class="product-item__hover-text">посмотреть товар</p>
                                            <img class="product-item__img" src="{{ ('front/images/content/product-1.png') }}" alt="">
                                            <h4 class="product-item__title">
                                                Водонепроницаемый
                                                Рюкзак
                                            </h4>
                                            <p class="price product-item__price">98 &#8372</p>
                                            <p class="product-item__notify-text">нет в наличии</p>
                                        </a>
                                    </div>
                                </div>
                                <div class="product-slider__item">
                                    <div class="product-item__wrapper">
                                        <button class="product-item__favorite"></button>
                                        <button class="product-item__basket">
                                            <img src="{{ ('front/images/basket-icon-white.svg') }}" alt="">
                                        </button>
                                        <a class="product-item__notify-link" href="#"><span>Сообщить о поступлении</span></a>
                                        <a class="product-item product-item--sale" href="#">
                                            <p class="product-item__hover-text">посмотреть товар</p>
                                            <img class="product-item__img" src="{{ ('front/images/content/product-1.png') }}" alt="">
                                            <h4 class="product-item__title">
                                                Водонепроницаемый
                                                Рюкзак
                                            </h4>
                                            <p class="price product-item__price">98 &#8372</p>
                                            <p class="product-item__notify-text">нет в наличии</p>
                                        </a>
                                    </div>
                                </div>
                                <div class="product-slider__item">
                                    <div class="product-item__wrapper">
                                        <button class="product-item__favorite"></button>
                                        <button class="product-item__basket">
                                            <img src="{{ asset('front/images/basket-icon-white.svg') }}" alt="">
                                        </button>
                                        <a class="product-item__notify-link" href="#"><span>Сообщить о поступлении</span></a>
                                        <a class="product-item product-item--sale" href="#">
                                            <p class="product-item__hover-text">посмотреть товар</p>
                                            <img class="product-item__img" src="{{ asset('front/images/content/product-2.png') }}" alt="">
                                            <h4 class="product-item__title">
                                                Водонепроницаемый
                                                Рюкзак
                                            </h4>
                                            <p class="price product-item__price">98 &#8372</p>
                                            <p class="product-item__notify-text">нет в наличии</p>
                                        </a>
                                    </div>
                                </div>
                                <div class="product-slider__item">
                                    <div class="product-item__wrapper">
                                        <button class="product-item__favorite"></button>
                                        <button class="product-item__basket">
                                            <img src="{{ asset('front/images/basket-icon-white.svg') }}" alt="">
                                        </button>
                                        <a class="product-item__notify-link" href="#"><span>Сообщить о поступлении</span></a>
                                        <a class="product-item product-item--sale" href="#">
                                            <p class="product-item__hover-text">посмотреть товар</p>
                                            <img class="product-item__img" src="{{ asset('front/images/content/product-1.png') }}" alt="">
                                            <h4 class="product-item__title">
                                                Водонепроницаемый
                                                Рюкзак
                                            </h4>
                                            <p class="price product-item__price">98 &#8372</p>
                                            <p class="product-item__notify-text">нет в наличии</p>
                                        </a>
                                    </div>
                                </div>
                                <div class="product-slider__item">
                                    <div class="product-item__wrapper">
                                        <button class="product-item__favorite"></button>
                                        <button class="product-item__basket">
                                            <img src="{{ asset('front/images/basket-icon-white.svg') }}" alt="">
                                        </button>
                                        <a class="product-item__notify-link" href="#"><span>Сообщить о поступлении</span></a>
                                        <a class="product-item product-item--sale" href="#">
                                            <p class="product-item__hover-text">посмотреть товар</p>
                                            <img class="product-item__img" src="{{ asset('front/images/content/product-1.png') }}" alt="">
                                            <h4 class="product-item__title">
                                                Водонепроницаемый
                                                Рюкзак
                                            </h4>
                                            <p class="price product-item__price">98 &#8372</p>
                                            <p class="product-item__notify-text">нет в наличии</p>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="related-tab-2" class="tabs-content products__content">
                            <div class="product-slider">
                                <div class="product-slider__item">
                                    <div class="product-item__wrapper">
                                        <button class="product-item__favorite"></button>
                                        <button class="product-item__basket">
                                            <img src="{{ asset('front/images/basket-icon-white.svg') }}" alt="">
                                        </button>
                                        <a class="product-item__notify-link" href="#"><span>Сообщить о поступлении</span></a>
                                        <a class="product-item" href="#">
                                            <p class="product-item__hover-text">посмотреть товар</p>
                                            <img class="product-item__img" src="{{ asset('front/images/content/product-2.png') }}" alt="">
                                            <h4 class="product-item__title">
                                                Водонепроницаемый
                                                Рюкзак
                                            </h4>
                                            <p class="price product-item__price">98 &#8372</p>
                                            <p class="product-item__notify-text">нет в наличии</p>
                                        </a>
                                    </div>
                                </div>
                                <div class="product-slider__item">
                                    <div class="product-item__wrapper">
                                        <button class="product-item__favorite"></button>
                                        <button class="product-item__basket">
                                            <img src="{{ asset('front/images/basket-icon-white.svg') }}" alt="">
                                        </button>
                                        <a class="product-item__notify-link" href="#"><span>Сообщить о поступлении</span></a>
                                        <a class="product-item product-item--sale" href="#">
                                            <p class="product-item__hover-text">посмотреть товар</p>
                                            <img class="product-item__img" src="{{ ('front/images/content/product-2.png') }}" alt="">
                                            <h4 class="product-item__title">
                                                Водонепроницаемый
                                                Рюкзак
                                            </h4>
                                            <p class="price product-item__price">98 &#8372</p>
                                            <p class="product-item__notify-text">нет в наличии</p>
                                        </a>
                                    </div>
                                </div>
                                <div class="product-slider__item">
                                    <div class="product-item__wrapper product-item__not-available">
                                        <button class="product-item__favorite"></button>
                                        <button class="product-item__basket">
                                            <img src="{{ asset('front/images/basket-icon-white.svg') }}" alt="">
                                        </button>
                                        <a class="product-item__notify-link" href="#"><span>Сообщить о поступлении</span></a>
                                        <a class="product-item product-item--sale" href="#">
                                            <p class="product-item__hover-text">посмотреть товар</p>
                                            <img class="product-item__img" src="{{ ('front/images/content/product-2.png') }}" alt="">
                                            <h4 class="product-item__title">
                                                Водонепроницаемый
                                                Рюкзак
                                            </h4>
                                            <p class="price product-item__price">98 &#8372</p>
                                            <p class="product-item__notify-text">нет в наличии</p>
                                        </a>
                                    </div>
                                </div>
                                <div class="product-slider__item">
                                    <div class="product-item__wrapper">
                                        <button class="product-item__favorite"></button>
                                        <button class="product-item__basket">
                                            <img src="{{ asset('front/images/basket-icon-white.svg') }}" alt="">
                                        </button>
                                        <a class="product-item__notify-link" href="#"><span>Сообщить о поступлении</span></a>
                                        <a class="product-item product-item--sale" href="#">
                                            <p class="product-item__hover-text">посмотреть товар</p>
                                            <img class="product-item__img" src="{{ asset('front/images/content/product-2.png') }}" alt="">
                                            <h4 class="product-item__title">
                                                Водонепроницаемый
                                                Рюкзак
                                            </h4>
                                            <p class="price product-item__price">98 &#8372</p>
                                            <p class="product-item__notify-text">нет в наличии</p>
                                        </a>
                                    </div>
                                </div>
                                <div class="product-slider__item">
                                    <div class="product-item__wrapper">
                                        <button class="product-item__favorite"></button>
                                        <button class="product-item__basket">
                                            <img src="{{ asset('front/images/basket-icon-white.svg') }}" alt="">
                                        </button>
                                        <a class="product-item__notify-link" href="#"><span>Сообщить о поступлении</span></a>
                                        <a class="product-item product-item--sale" href="#">
                                            <p class="product-item__hover-text">посмотреть товар</p>
                                            <img class="product-item__img" src="{{ asset('front/images/content/product-2.png') }}" alt="">
                                            <h4 class="product-item__title">
                                                Водонепроницаемый
                                                Рюкзак
                                            </h4>
                                            <p class="price product-item__price">98 &#8372</p>
                                            <p class="product-item__notify-text">нет в наличии</p>
                                        </a>
                                    </div>
                                </div>
                                <div class="product-slider__item">
                                    <div class="product-item__wrapper">
                                        <button class="product-item__favorite"></button>
                                        <button class="product-item__basket">
                                            <img src="{{ asset('front/images/basket-icon-white.svg') }}" alt="">
                                        </button>
                                        <a class="product-item__notify-link" href="#"><span>Сообщить о поступлении</span></a>
                                        <a class="product-item product-item--sale" href="#">
                                            <p class="product-item__hover-text">посмотреть товар</p>
                                            <img class="product-item__img" src="{{ asset('front/images/content/product-2.png') }}" alt="">
                                            <h4 class="product-item__title">
                                                Водонепроницаемый
                                                Рюкзак
                                            </h4>
                                            <p class="price product-item__price">98 &#8372</p>
                                            <p class="product-item__notify-text">нет в наличии</p>
                                        </a>
                                    </div>
                                </div>
                                <div class="product-slider__item">
                                    <div class="product-item__wrapper">
                                        <button class="product-item__favorite"></button>
                                        <button class="product-item__basket">
                                            <img src="{{ asset('front/images/basket-icon-white.svg') }}" alt="">
                                        </button>
                                        <a class="product-item__notify-link" href="#"><span>Сообщить о поступлении</span></a>
                                        <a class="product-item product-item--sale" href="#">
                                            <p class="product-item__hover-text">посмотреть товар</p>
                                            <img class="product-item__img" src="{{ asset('front/images/content/product-2.png') }}" alt="">
                                            <h4 class="product-item__title">
                                                Водонепроницаемый
                                                Рюкзак
                                            </h4>
                                            <p class="price product-item__price">98 &#8372</p>
                                            <p class="product-item__notify-text">нет в наличии</p>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="related-tab-3" class="tabs-content products__content">Slider 3</div>
                        <div id="related-tab-4" class="tabs-content products__content">Slider 4</div>
                        <div id="related-tab-5" class="tabs-content products__content">Slider 5</div>
                        <div id="related-tab-6" class="tabs-content products__content">Slider 6</div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
