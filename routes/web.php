<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [\App\Http\Controllers\Front\MainController::class, 'home'])->name('home');

Route::get('/catalog', [\App\Http\Controllers\Front\MainController::class, 'catalog'])->name('catalog');
Route::get('/product-page', [\App\Http\Controllers\Front\MainController::class, 'productPage'])->name('product-page');


Route::get('/admin', [\App\Http\Controllers\Admin\MainController::class, 'dashboard'])->name('dashboard');
