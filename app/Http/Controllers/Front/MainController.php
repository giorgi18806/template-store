<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function home()
    {
        return view('front.home');
    }

    public function catalog()
    {
        return view('front.pages.catalog');
    }

    public function productPage()
    {
        return view('front.pages.product-page');
    }
}
